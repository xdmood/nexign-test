package com.nexign.slc.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class SubscriberAction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscriber_action_id_gen")
    @SequenceGenerator(name = "subscriber_action_id_gen", sequenceName = "subscriber_action_id_seq", allocationSize = 1)
    private Long id;

    @NotNull
    private Integer subscriberId;

    @NotNull
    private Integer actionId;

    @NotNull
    private BigDecimal cost;

    @NotNull
    private LocalDate actionDate;

    @NotNull
    private LocalTime actionTime;
}

