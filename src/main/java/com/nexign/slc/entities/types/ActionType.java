package com.nexign.slc.entities.types;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ActionType {

    PAYMENT("PAYMENT"),
    REPLENISHMENT("REPLENISHMENT");

    private final String value;

    ActionType(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
