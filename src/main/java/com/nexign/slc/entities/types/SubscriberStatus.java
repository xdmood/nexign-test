package com.nexign.slc.entities.types;

import com.fasterxml.jackson.annotation.JsonValue;

public enum SubscriberStatus {

    ACTIVE("ACTIVE"),
    BLOCKED("BLOCKED");

    private final String value;

    SubscriberStatus(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}

