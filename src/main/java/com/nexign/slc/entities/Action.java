package com.nexign.slc.entities;

import com.nexign.slc.entities.types.ActionType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Action {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "action_seq_gen")
    @SequenceGenerator(name = "action_seq_gen", sequenceName = "action_id_seq", allocationSize = 1)
    private Integer id;

    @NotBlank
    private String name;

    @NotNull
    private BigDecimal cost;

    @NotNull
    private Integer dayLimit;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ActionType type;
}
