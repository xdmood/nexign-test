package com.nexign.slc.services;

import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.Subscriber;
import com.nexign.slc.entities.types.SubscriberStatus;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.repositories.SubscriberRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Slf4j
@Service
public class SubscriberServiceImpl implements SubscriberService {

    @Autowired
    private SubscriberRepository subscriberRepository;

    @Value("${slc.subscriber.msisdn.length.min:7}")
    private int msisdnLengthMin;

    @Value("${slc.subscriber.msisdn.length.max:15}")
    private int msisdnLengthMax;


    @Override
    public Subscriber getSubscriber(Integer id) throws SlcException {
        return subscriberRepository.findById(id).orElseThrow(SlcException::new);
    }

    @Override
    public Subscriber getSubscriberByMsisdn(BigInteger msisdn) throws SlcException {
        return subscriberRepository.findByMsisdn(msisdn).orElseThrow(SlcException::new);
    }

    @Override
    public List<Subscriber> getSubscribers() {
        return subscriberRepository.findAll();
    }

    @Override
    public Subscriber save(Subscriber subscriber) throws SlcException {

        if (subscriber == null)
            throw new SlcException("Subscriber cannot be null", HttpStatus.BAD_REQUEST);

        if (subscriber.getId() == null) {
            log.info("Create new subscriber : value: {}", subscriber);

            // Validate value
            String errorMessage = validateSubscriber(subscriber);
            if (!StringUtils.isEmpty(errorMessage))
                throw new SlcException(errorMessage, HttpStatus.BAD_REQUEST);

            // Check MSISDN in DB
            Subscriber foundSubscriber = subscriberRepository.findByMsisdn(subscriber.getMsisdn()).orElse(null);
            if (foundSubscriber != null)
                throw new SlcException("An entity with the same \"msisdn\" already exists!", HttpStatus.CONFLICT);

            // Set balance and status
            subscriber.setBalance(new BigDecimal(0));
            subscriber.setStatus(SubscriberStatus.ACTIVE);

            // Save value
            subscriberRepository.save(subscriber);
            log.info("Create new subscriber : saved");

            return subscriberRepository.findByMsisdn(subscriber.getMsisdn()).orElseThrow(SlcException::new);
        } else {
            log.info("Update subscriber : new value: {}", subscriber);

            // Check msisdn
            if (subscriber.getMsisdn().signum() == -1)
                throw new SlcException("msisdn have negative value!", HttpStatus.BAD_REQUEST);

            // Check msisdn length
            if (subscriber.getMsisdn().toString().length() < msisdnLengthMin || subscriber.getMsisdn().toString().length() > msisdnLengthMax)
                throw new SlcException("msisdn have wrong length!", HttpStatus.BAD_REQUEST);

            // Get subscriber
            Subscriber updateSubscriber = subscriberRepository.findById(subscriber.getId()).orElseThrow(SlcException::new);
            log.info("Update subscriber : id: {}", updateSubscriber.getId());

            // Check MSISDN in DB
            if (!updateSubscriber.getMsisdn().equals(subscriber.getMsisdn())) {
                Subscriber foundSubscriber = subscriberRepository.findByMsisdn(subscriber.getMsisdn()).orElse(null);
                if (foundSubscriber != null)
                    throw new SlcException("An entity with the same \"msisdn\" already exists!", HttpStatus.CONFLICT);
            }

            // Set new value
            updateSubscriber.setFirstName(StringUtils.isEmpty(subscriber.getFirstName()) ? updateSubscriber.getFirstName() : subscriber.getFirstName());
            updateSubscriber.setLastName(StringUtils.isEmpty(subscriber.getLastName()) ? updateSubscriber.getLastName() : subscriber.getLastName());
            updateSubscriber.setBalance(subscriber.getBalance() == null ? updateSubscriber.getBalance() : subscriber.getBalance());
            updateSubscriber.setMsisdn(subscriber.getMsisdn() == null ? updateSubscriber.getMsisdn() : subscriber.getMsisdn());
            updateSubscriber.setStatus(subscriber.getStatus() == null ? updateSubscriber.getStatus() : subscriber.getStatus());

            // Save value
            subscriberRepository.save(updateSubscriber);
            log.info("Update subscriber : successful");

            return updateSubscriber;
        }
    }

    @Override
    public void changeBalance(Subscriber subscriber, Action action, BigDecimal cost) throws SlcException {

        if (subscriber == null || action == null || cost == null || cost.signum() == -1)
            throw new SlcException("The subscriber or action not be null. Cost not be negative value", HttpStatus.BAD_REQUEST);

        log.info("Change subscriber balance: {} : action: {}", subscriber, action);

        // Check action type
        switch (action.getType()) {
            case PAYMENT:
                subscriber.setBalance(subscriber.getBalance().subtract(cost));
                break;
            case REPLENISHMENT:
                subscriber.setBalance(subscriber.getBalance().add(cost));
                break;
            default:
                throw new SlcException("The action type is not supported.", HttpStatus.BAD_REQUEST);
        }

        // Check and change status
        changeStatus(subscriber);

        // Save changes
        save(subscriber);
    }

    @Override
    public void changeStatus(Subscriber subscriber) throws SlcException {

        if (subscriber == null)
            throw new SlcException("The subscriber not be null.", HttpStatus.BAD_REQUEST);

        log.info("Change subscriber status: {}", subscriber);

        // Check and change status
        if (subscriber.getBalance().signum() == -1 && subscriber.getStatus() != SubscriberStatus.BLOCKED) {
            log.info("Subscriber have negative balance. Status changed to: BLOCKED");
            subscriber.setStatus(SubscriberStatus.BLOCKED);
        } else if (subscriber.getBalance().signum() >= 0 && subscriber.getStatus() != SubscriberStatus.ACTIVE) {
            log.info("Subscriber have positive balance. Status changed to: ACTIVE");
            subscriber.setStatus(SubscriberStatus.ACTIVE);
        }

    }


    private String validateSubscriber(Subscriber subscriber) {

        String errors = "";

        if (StringUtils.isEmpty(subscriber.getFirstName()))
            errors += ": firstName";

        if (StringUtils.isEmpty(subscriber.getLastName()))
            errors += ": lastName";

        if (subscriber.getMsisdn() == null || subscriber.getMsisdn().signum() == -1)
            errors += ": msisdn";
        else if (subscriber.getMsisdn().toString().length() < msisdnLengthMin || subscriber.getMsisdn().toString().length() > msisdnLengthMax)
            errors += ": msisdn have wrong length!";

        if (!StringUtils.isEmpty(errors))
            errors = "Values cannot be null, empty or negative value" + errors;

        return errors;
    }
}
