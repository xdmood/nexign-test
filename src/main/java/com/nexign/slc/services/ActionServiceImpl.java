package com.nexign.slc.services;

import com.nexign.slc.entities.Action;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.repositories.ActionRepository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Slf4j
@Service
public class ActionServiceImpl implements ActionService {

    @Autowired
    private ActionRepository actionRepository;

    @Override
    public Action getAction(Integer id) throws SlcException {
        return actionRepository.findById(id).orElseThrow(SlcException::new);
    }

    @Override
    public Action getAction(String name) throws SlcException {
        return actionRepository.findByName(name).orElseThrow(SlcException::new);
    }

    @Override
    public List<Action> getActions() {
        return actionRepository.findAll();
    }

    @Override
    public Action save(Action action) throws SlcException {

        if (action == null)
            throw new SlcException("Action cannot be null", HttpStatus.BAD_REQUEST);

        if (action.getId() == null) {
            log.info("Create new action : value: {}", action);

            // Validate value
            String errorMessage = validateAction(action);
            if (!StringUtils.isEmpty(errorMessage))
                throw new SlcException(errorMessage, HttpStatus.BAD_REQUEST);

            // Check name in DB
            Action foundAction = actionRepository.findByName(action.getName()).orElse(null);
            if (foundAction != null)
                throw new SlcException("An entity with the same \"name\" already exists!", HttpStatus.CONFLICT);

            // Save value
            actionRepository.save(action);
            log.info("Create new action : saved");

            return actionRepository.findByName(action.getName()).orElseThrow(SlcException::new);
        } else {
            log.info("Update action : new value: {}", action);

            // Get Action
            Action updateAction = actionRepository.findById(action.getId()).orElseThrow(SlcException::new);
            log.info("Update action : id: {}", updateAction.getId());

            // Check cost
            if (action.getCost() != null && action.getCost().signum() == -1)
                throw new SlcException("Cost cannot have negative value!", HttpStatus.BAD_REQUEST);

            // Check name in DB
            if (!updateAction.getName().equalsIgnoreCase(action.getName())) {
                Action foundAction = actionRepository.findByName(action.getName()).orElse(null);
                if (foundAction != null && !foundAction.getId().equals(action.getId()))
                    throw new SlcException("An entity with the same \"name\" already exists!", HttpStatus.CONFLICT);
            }

            // Set new value
            updateAction.setName(StringUtils.isEmpty(action.getName()) ? updateAction.getName(): action.getName());
            updateAction.setCost(action.getCost() == null ? updateAction.getCost(): action.getCost());
            updateAction.setDayLimit(action.getDayLimit() == null ? updateAction.getDayLimit(): action.getDayLimit());
            updateAction.setType(action.getType() == null ? updateAction.getType(): action.getType());

            // Save value
            actionRepository.save(updateAction);
            log.info("Update action : successful");

            return updateAction;
        }
    }


    private String validateAction(Action action) {

        String errors = "";

        if (StringUtils.isEmpty(action.getName()))
            errors += ": name";

        if (action.getCost() == null || action.getCost().signum() == -1)
            errors += ": cost";

        if (action.getDayLimit() == null)
            errors += ": dayLimit";

        if (action.getType() == null)
            errors += ": type";

        if (!StringUtils.isEmpty(errors))
            errors = "Values cannot be null or empty. Cost cannot have negative value" + errors;

        return errors;
    }
}