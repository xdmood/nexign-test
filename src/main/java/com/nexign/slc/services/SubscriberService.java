package com.nexign.slc.services;

import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.Subscriber;
import com.nexign.slc.exception.SlcException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public interface SubscriberService {

    /**
     * Get subscriber by id
     */
    Subscriber getSubscriber(Integer id) throws SlcException;

    /**
     * Get subscriber by msisdn
     */
    Subscriber getSubscriberByMsisdn(BigInteger msisdn) throws SlcException;

    /**
     * Get all subscribers
     */
    List<Subscriber> getSubscribers();

    /**
     * Save or update subscribers
     *
     * @param subscriber entity
     * @return new subscriber
     * @throws SlcException record not found
     */
    Subscriber save(Subscriber subscriber) throws SlcException;

    /**
     * Change balance for the subscriber
     */
    void changeBalance(Subscriber subscriber, Action action, BigDecimal cost) throws SlcException;


    /**
     * Change status for the subscriber
     */
    void changeStatus(Subscriber subscriber) throws SlcException;
}