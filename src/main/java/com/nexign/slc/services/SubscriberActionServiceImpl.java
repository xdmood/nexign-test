package com.nexign.slc.services;

import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.SubscriberAction;
import com.nexign.slc.entities.Subscriber;
import com.nexign.slc.entities.types.ActionType;
import com.nexign.slc.entities.types.SubscriberStatus;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.models.SubscriberActionResultResponse;
import com.nexign.slc.repositories.SubscriberActionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Slf4j
@Service
public class SubscriberActionServiceImpl implements SubscriberActionService {

    @Autowired
    private SubscriberActionRepository subscriberActionRepository;

    @Autowired
    private SubscriberServiceImpl subscriberService;

    @Autowired
    private ActionServiceImpl actionService;


    @Override
    public SubscriberAction getSubscriberAction(Long id) throws SlcException {
        return subscriberActionRepository.findById(id).orElseThrow(SlcException::new);
    }

    @Override
    public List<SubscriberAction> getSubscriberActions() {
        return subscriberActionRepository.findAll();
    }

    @Override
    public List<SubscriberAction> getSubscriberActions(Integer subscriberId) {
        return subscriberActionRepository.findAllBySubscriberId(subscriberId);
    }

    @Override
    public List<SubscriberAction> getSubscriberActions(Integer actionId, Integer subscriberId) {
        return subscriberActionRepository.findAllByActionIdAndSubscriberId(actionId, subscriberId);
    }

    @Override
    public List<SubscriberAction> getSubscriberActions(Integer actionId, Integer subscriberId, LocalDate actionDate) {
        return subscriberActionRepository.findAllByActionIdAndSubscriberIdAndActionDate(actionId, subscriberId, actionDate);
    }

    @Override
    public SubscriberActionResultResponse save(SubscriberAction subscriberAction, BigDecimal cost) throws SlcException {

        // Validate value
        if (subscriberAction == null)
            throw new SlcException("SubscriberAction cannot be null", HttpStatus.BAD_REQUEST);

        // Check actionId and subscriberId
        if (subscriberAction.getActionId() == null)
            throw new SlcException("SubscriberAction actionId cannot be null", HttpStatus.BAD_REQUEST);
        if (subscriberAction.getSubscriberId() == null)
            throw new SlcException("SubscriberAction subscriberId cannot be null", HttpStatus.BAD_REQUEST);

        // Get action
        log.info("Find action in DB by id: {}", subscriberAction.getActionId());
        Action action = actionService.getAction(subscriberAction.getActionId());

        // Get subscriber
        log.info("Find subscriber in DB by id: {}", subscriberAction.getSubscriberId());
        Subscriber subscriber = subscriberService.getSubscriber(subscriberAction.getSubscriberId());

        // Check subscriber status
        if (subscriber.getStatus() == SubscriberStatus.BLOCKED && action.getType() == ActionType.PAYMENT)
            return SubscriberActionResultResponse.builder()
                    .result(false)
                    .message("Subscriber status: BLOCKED")
                    .build();

        // Check action limit
        if (action.getDayLimit() >= 0) {
            List<SubscriberAction> subscriberActions = subscriberActionRepository.findAllByActionIdAndSubscriberIdAndActionDate(
                    action.getId(),
                    subscriber.getId(),
                    LocalDate.now());

            if (subscriberActions.size() >= action.getDayLimit())
                return SubscriberActionResultResponse.builder().result(false).message("Daily action limit exceeded. Please, try again later").build();
        }

        // Set payment cost
        if (action.getType() == ActionType.PAYMENT)
            cost = action.getCost();

        // Set subscriber action params
        subscriberAction.setActionDate(LocalDate.now());
        subscriberAction.setActionTime(LocalTime.now());
        subscriberAction.setSubscriberId(subscriber.getId());
        subscriberAction.setActionId(action.getId());
        subscriberAction.setCost(cost);

        // Change subscriber balance
        subscriberService.changeBalance(subscriber, action, cost);

        // Save subscriber actions
        subscriberActionRepository.save(subscriberAction);

        return SubscriberActionResultResponse.builder().result(true).message("ok").build();
    }
}
