package com.nexign.slc.services;

import com.nexign.slc.entities.Action;
import com.nexign.slc.exception.SlcException;

import java.util.List;

public interface ActionService {

    /**
     * Get Action by id
     */
    Action getAction(Integer id) throws SlcException;

    /**
     * Get Action by name
     */
    Action getAction(String name) throws SlcException;

    /**
     * Get all actions
     */
    List<Action> getActions();

    /**
     * Save or update actions
     *
     * @param action entity
     * @return new action
     * @throws SlcException record not found
     */
    Action save(Action action) throws SlcException;
}
