package com.nexign.slc.services;

import com.nexign.slc.entities.SubscriberAction;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.models.SubscriberActionResultResponse;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface SubscriberActionService {

    /**
     * Get subscriber action by id
     */
    SubscriberAction getSubscriberAction(Long id) throws SlcException;

    /**
     * Get all subscriber actions
     */
    List<SubscriberAction> getSubscriberActions();

    /**
     * Get subscriber actions by subscriberId
     */
    List<SubscriberAction> getSubscriberActions(Integer subscriberId);

    /**
     * Get subscriber actions by subscriberId and actionId
     */
    List<SubscriberAction> getSubscriberActions(Integer actionId, Integer subscriberId);

    /**
     * Get subscriber actions by subscriberId and actionId and actionTime
     */
    List<SubscriberAction> getSubscriberActions(Integer actionId, Integer subscriberId, LocalDate actionDate);

    /**
     * Save subscriber actions
     *
     * @param subscriberAction entity
     * @param cost custom cost
     * @throws SlcException record not found
     */
    SubscriberActionResultResponse save(SubscriberAction subscriberAction, BigDecimal cost) throws SlcException;
}
