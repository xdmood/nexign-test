package com.nexign.slc.controllers;

import com.nexign.slc.dto.ActionRequestDto;
import com.nexign.slc.dto.ActionResponseDto;
import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.types.ActionType;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.services.ActionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "ACTION")
@Slf4j
@RestController
@RequestMapping("/api/v1/actions")
public class ActionController {

    private ActionService actionServiceImpl;
    private ModelMapper modelMapper;


    public ActionController(ActionService actionServiceImpl, ModelMapper modelMapper) {
        this.actionServiceImpl = actionServiceImpl;
        this.modelMapper = modelMapper;
    }


    @ApiOperation(value = "Список действий доступных абоненту", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @GetMapping(produces = {"application/json"})
    public ResponseEntity getActions() {
        log.info("REQUEST : Get actions");
        return ResponseEntity.ok().body(actionServiceImpl.getActions().stream()
                .map(action -> modelMapper.map(action, ActionResponseDto.class))
                .collect(Collectors.toList()));
    }

    @ApiOperation(value = "Список типов действий доступных абоненту", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @GetMapping(value = "/types", produces = {"application/json"})
    public ResponseEntity getActionTypes() {
        log.info("REQUEST : Get action types");
        return ResponseEntity.ok().body(Arrays.asList(ActionType.values()));
    }

    @ApiOperation(value = "Получить действие по \"id\"", response = ActionResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @GetMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity getAction(@PathVariable Integer id) throws SlcException {
        log.info("REQUEST : Get action by ID: {}", id);
        return ResponseEntity.ok().body(modelMapper.map(actionServiceImpl.getAction(id), ActionResponseDto.class));
    }

    @ApiOperation(value = "Создать действие", response = ActionResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 409, message = "Conflict - name must be unique"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @PostMapping(produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity createAction(@RequestBody ActionRequestDto actionRequestDto) throws SlcException {
        log.info("REQUEST : Create action : value: {}", actionRequestDto);
        Action action = modelMapper.map(actionRequestDto, Action.class);
        return ResponseEntity.status(HttpStatus.CREATED).body(modelMapper.map(actionServiceImpl.save(action), ActionResponseDto.class));
    }

    @ApiOperation(value = "Обновить атрибуты действия", response = ActionResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 409, message = "Conflict - name must be unique"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @PutMapping(value = "/{id}", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity updateAction(@PathVariable Integer id, @RequestBody ActionRequestDto actionRequestDto) throws SlcException {
        log.info("REQUEST : Update action : id: {} : value: {}", id, actionRequestDto);
        Action action = modelMapper.map(actionRequestDto, Action.class);
        action.setId(id);
        return ResponseEntity.ok().body(modelMapper.map(actionServiceImpl.save(action), ActionResponseDto.class));
    }
}
