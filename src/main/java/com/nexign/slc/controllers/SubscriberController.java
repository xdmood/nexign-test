package com.nexign.slc.controllers;

import com.nexign.slc.dto.SubscriberActionRequestDto;
import com.nexign.slc.dto.SubscriberActionResponseDto;
import com.nexign.slc.dto.SubscriberRequestDto;
import com.nexign.slc.dto.SubscriberResponseDto;
import com.nexign.slc.entities.Subscriber;
import com.nexign.slc.entities.SubscriberAction;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.models.SubscriberActionResultResponse;
import com.nexign.slc.services.SubscriberActionService;
import com.nexign.slc.services.SubscriberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "SUBSCRIBER")
@Slf4j
@RestController
@RequestMapping("/api/v1/subscribers")
public class SubscriberController {

    private SubscriberService subscriberServiceImpl;
    private SubscriberActionService subscriberActionServiceImpl;
    private ModelMapper modelMapper;

    public SubscriberController(SubscriberService subscriberServiceImpl,
                                SubscriberActionService subscriberActionServiceImpl,
                                ModelMapper modelMapper) {
        this.subscriberServiceImpl = subscriberServiceImpl;
        this.subscriberActionServiceImpl = subscriberActionServiceImpl;
        this.modelMapper = modelMapper;
    }

    @ApiOperation(value = "Список абонентов", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @GetMapping(produces = {"application/json"})
    public ResponseEntity getSubscribers() {
        log.info("REQUEST : Get subscribers");
        return ResponseEntity.ok().body(subscriberServiceImpl.getSubscribers().stream()
                .map(subscriber -> modelMapper.map(subscriber, SubscriberResponseDto.class))
                .collect(Collectors.toList()));
    }

    @ApiOperation(value = "Получить абонента по \"id\"", response = SubscriberResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @GetMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity getSubscriber(@PathVariable Integer id) throws SlcException {
        log.info("REQUEST : Get subscriber by ID: {}", id);
        return ResponseEntity.ok().body(modelMapper.map(subscriberServiceImpl.getSubscriber(id), SubscriberResponseDto.class));
    }

    @ApiOperation(value = "Создать абонента", response = SubscriberResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 409, message = "Conflict - msisdn must be unique"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @PostMapping(produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity createSubscriber(@RequestBody SubscriberRequestDto subscriberRequestDto) throws SlcException {
        log.info("REQUEST : Create subscriber : value: {}", subscriberRequestDto);
        Subscriber subscriber = modelMapper.map(subscriberRequestDto, Subscriber.class);
        return ResponseEntity.status(HttpStatus.CREATED).body(modelMapper.map(subscriberServiceImpl.save(subscriber), SubscriberResponseDto.class));
    }

    @ApiOperation(value = "Обновить атрибуты абонента", response = SubscriberResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 409, message = "Conflict - msisdn must be unique"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @PutMapping(value = "/{id}", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity updateSubscriber(@PathVariable Integer id, @RequestBody SubscriberRequestDto subscriberRequestDto) throws SlcException {
        log.info("REQUEST : Update subscriber : value: {}", subscriberRequestDto);
        Subscriber subscriber = modelMapper.map(subscriberRequestDto, Subscriber.class);
        subscriber.setId(id);
        return ResponseEntity.ok().body(modelMapper.map(subscriberServiceImpl.save(subscriber), SubscriberResponseDto.class));
    }

    @ApiOperation(value = "Создать действие абонента (звонок, смс, поплнение баланса)", response = SubscriberActionResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Subscriber action not created, subscriber have status: BLOCKED"),
            @ApiResponse(code = 201, message = "Subscriber action created"),
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @PostMapping(value = "/{id}/actions", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity createSubscriberActions(@PathVariable Integer id, @RequestBody SubscriberActionRequestDto subscriberActionRequestDto) throws SlcException {
        log.info("REQUEST : Create subscriber action : value: {}", subscriberActionRequestDto);
        SubscriberAction subscriberAction = modelMapper.map(subscriberActionRequestDto, SubscriberAction.class);
        subscriberAction.setSubscriberId(id);
        SubscriberActionResultResponse subscriberActionResultResponse = subscriberActionServiceImpl.save(subscriberAction, subscriberActionRequestDto.getCost());
        return ResponseEntity
                .status(subscriberActionResultResponse.isResult() ? HttpStatus.CREATED : HttpStatus.OK)
                .body(subscriberActionResultResponse);
    }

    @ApiOperation(value = "Получить список действий абонента по \"id\"", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 404, message = "Resource not found"),
            @ApiResponse(code = 500, message = "Something went wrong in server layer")})
    @GetMapping(value = "/{id}/actions", produces = {"application/json"})
    public ResponseEntity getSubscriberActions(@PathVariable Integer id) {
        log.info("REQUEST : Get subscriber actions by subscriberId: {}", id);
        return ResponseEntity.ok().body(subscriberActionServiceImpl.getSubscriberActions(id).stream()
                .map(subscriberAction -> modelMapper.map(subscriberAction, SubscriberActionResponseDto.class))
                .collect(Collectors.toList()));
    }
}
