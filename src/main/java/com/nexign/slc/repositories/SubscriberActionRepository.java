package com.nexign.slc.repositories;

import com.nexign.slc.entities.SubscriberAction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface SubscriberActionRepository extends JpaRepository<SubscriberAction, Long> {

    List<SubscriberAction> findAllBySubscriberId(Integer subscriberId);

    List<SubscriberAction> findAllByActionIdAndSubscriberId(Integer actionId, Integer subscriberId);

    List<SubscriberAction> findAllByActionIdAndSubscriberIdAndActionDate(Integer actionId, Integer subscriberId, LocalDate actionDate);
}