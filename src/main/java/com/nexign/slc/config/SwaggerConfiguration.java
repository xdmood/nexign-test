package com.nexign.slc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Optional;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.nexign.slc.controllers"))
                .build()
                .apiInfo(metadata())
                .useDefaultResponseMessages(false)
                .tags(new Tag("SUBSCRIBER", "Работа с сущностью \"Абонент\""))
                .tags(new Tag("ACTION", "Работа с сущностью \"Действие\""))
                .genericModelSubstitutes(Optional.class);
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("API приложения \"Жизненный цикл абонента\"")
                .description("ъуъ")
                .version("1.0")
                .contact(new Contact(null, null, "xdmood@gmail.com"))
                .build();
    }
}
