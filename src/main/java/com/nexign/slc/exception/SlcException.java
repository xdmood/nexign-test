package com.nexign.slc.exception;

import org.springframework.http.HttpStatus;

public class SlcException extends Exception {

    private final String message;
    private final HttpStatus httpStatus;

    public SlcException() {
        message = "SQL record not found";
        httpStatus = HttpStatus.NOT_FOUND;
    }

    public SlcException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
