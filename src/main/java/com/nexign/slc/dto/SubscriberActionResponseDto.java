package com.nexign.slc.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class SubscriberActionResponseDto implements Serializable {

    @ApiModelProperty
    private Long id;

    @ApiModelProperty(position = 1)
    private Integer subscriberId;

    @ApiModelProperty(position = 2)
    private Integer actionId;

    @ApiModelProperty(position = 3)
    private BigDecimal cost;

    @ApiModelProperty(position = 4)
    private LocalDate actionDate;

    @ApiModelProperty(position = 5)
    private LocalTime actionTime;
}
