package com.nexign.slc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.nexign.slc.entities.types.ActionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActionResponseDto implements Serializable {

    @ApiModelProperty
    private Integer id;

    @ApiModelProperty(position = 1)
    private String name;

    @ApiModelProperty(position = 2)
    private BigDecimal cost;

    @ApiModelProperty(position = 3)
    private Integer dayLimit;

    @ApiModelProperty(position = 4)
    private ActionType type;

}
