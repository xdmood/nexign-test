package com.nexign.slc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.nexign.slc.entities.types.SubscriberStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubscriberRequestDto implements Serializable {

    @ApiModelProperty
    private String firstName;

    @ApiModelProperty(position = 1)
    private String lastName;

    @ApiModelProperty(position = 2)
    private BigInteger msisdn;
}
