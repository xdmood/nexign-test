package com.nexign.slc.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexign.slc.SlcApplication;
import com.nexign.slc.dto.ActionRequestDto;
import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.types.ActionType;
import com.nexign.slc.repositories.ActionRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SlcApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ActionControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private ModelMapper modelMapper;

    // Jackson mapper
    private final ObjectMapper mapper = new ObjectMapper();

    // Set of test action names
    private Set<String> actionNames = new HashSet<>();


    @Transactional
    @AfterEach
    void resetDb() {
        actionRepository.findAll().stream()
                .filter(action -> actionNames.contains(action.getName()))
                .forEach(action -> actionRepository.deleteById(action.getId()));
    }

    @Test
    void givenActionsWhenGetActionsThenStatus200() throws Exception {
        actionRepository.save(createTestAction("test_1"));
        actionRepository.save(createTestAction("test_2"));

        mvc.perform(get("/api/v1/actions")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(2))))
                .andExpect(jsonPath("$[?(@.name == \"test_1\")].name", hasItems("test_1")))
                .andExpect(jsonPath("$[?(@.name == \"test_2\")].name", hasItems("test_2")));
    }

    @Test
    void givenActionByIdWhenGetActionThenStatus200() throws Exception {
        actionRepository.save(createTestAction("test"));
        Action actionTest = actionRepository.findByName("test").orElseThrow(Exception::new);
        String url = "/api/v1/actions/" + actionTest.getId();

        mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("test")));
    }

    @Test
    void givenActionByIdWhenGetActionThenStatus404() throws Exception {
        mvc.perform(get("/api/v1/actions/-102030")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void whenValidInputThenCreateActionThenStatus201() throws Exception {
        Action actionTest = createTestAction("test");

        mvc.perform(post("/api/v1/actions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(actionTest, ActionRequestDto.class))))
                .andDo(print())
                .andExpect(status().isCreated());

        List<Action> actions = actionRepository.findAll();
        assertThat(actions).extracting(Action::getName).contains(actionTest.getName());
    }

    @Test
    void whenInvalidInputThenCreateActionThenStatus400() throws Exception {
        Action actionTest = createTestAction("test");
        actionTest.setName(null);

        mvc.perform(post("/api/v1/actions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(actionTest, ActionRequestDto.class))))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidInputThenCreateActionThenStatus409() throws Exception {
        Action actionTest = createTestAction("test");
        actionRepository.save(actionTest);

        mvc.perform(post("/api/v1/actions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(actionTest, ActionRequestDto.class))))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    void whenValidInputThenUpdateActionThenStatus200() throws Exception {
        actionRepository.save(createTestAction("test"));
        Action actionTest = actionRepository.findByName("test").orElseThrow(Exception::new);
        actionTest.setName("test_update");
        actionNames.add("test_update");
        String url = "/api/v1/actions/" + actionTest.getId();

        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(actionTest, ActionRequestDto.class))))
                .andDo(print())
                .andExpect(status().isOk());

        List<Action> actions = actionRepository.findAll();
        assertThat(actions).extracting(Action::getName).contains(actionTest.getName());
    }

    @Test
    void whenInvalidInputThenUpdateActionThenStatus404() throws Exception {
        Action actionTest = createTestAction("test");

        mvc.perform(put("/api/v1/actions/-102030")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(actionTest, ActionRequestDto.class))))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void whenInvalidInputThenUpdateActionThenStatus409() throws Exception {
        actionRepository.save(createTestAction("test"));
        actionRepository.save(createTestAction("test_conflict"));

        Action actionTest = actionRepository.findByName("test").orElseThrow(Exception::new);
        actionTest.setName("test_conflict");
        String url = "/api/v1/actions/" + actionTest.getId();

        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(actionTest, ActionRequestDto.class))))
                .andDo(print())
                .andExpect(status().isConflict());
    }


    private Action createTestAction(String name) {
        actionNames.add(name);
        return Action.builder()
                .name(name)
                .cost(new BigDecimal("0.00"))
                .dayLimit(1)
                .type(ActionType.PAYMENT)
                .build();
    }
}
