package com.nexign.slc.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexign.slc.SlcApplication;
import com.nexign.slc.dto.SubscriberActionRequestDto;
import com.nexign.slc.dto.SubscriberRequestDto;
import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.Subscriber;
import com.nexign.slc.entities.SubscriberAction;
import com.nexign.slc.entities.types.ActionType;
import com.nexign.slc.entities.types.SubscriberStatus;
import com.nexign.slc.repositories.ActionRepository;
import com.nexign.slc.repositories.SubscriberActionRepository;
import com.nexign.slc.repositories.SubscriberRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SlcApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SubscriberControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private SubscriberRepository subscriberRepository;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private SubscriberActionRepository subscriberActionRepository;

    @Autowired
    private ModelMapper modelMapper;

    // Jackson mapper
    private final ObjectMapper mapper = new ObjectMapper();

    // Set of test subscribers names
    private Set<String> subscriberMsisdns = new HashSet<>();

    // Set of test action names
    private Set<String> actionNames = new HashSet<>();

    @Transactional
    @AfterEach
    void resetDb() {
        subscriberRepository.findAll().stream()
                .filter(subscriber -> subscriberMsisdns.contains(subscriber.getMsisdn().toString()))
                .forEach(subscriber -> subscriberRepository.deleteById(subscriber.getId()));

        actionRepository.findAll().stream()
                .filter(action -> actionNames.contains(action.getName()))
                .forEach(action -> actionRepository.deleteById(action.getId()));
    }

    @Test
    void givenSubscribersWhenGetSubscribersThenStatus200() throws Exception {
        subscriberRepository.save(createTestSubscriber("Bob", "Smith", "79217776655"));
        subscriberRepository.save(createTestSubscriber("Jack", "Williams", "79217776644"));

        mvc.perform(get("/api/v1/subscribers")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(2))))
                .andExpect(jsonPath("$[?(@.firstName == \"Bob\")].msisdn", hasItem(79217776655L)))
                .andExpect(jsonPath("$[?(@.firstName == \"Jack\")].msisdn", hasItem(79217776644L)));
    }

    @Test
    void givenSubscriberByIdWhenGetSubscriberThenStatus200() throws Exception {
        subscriberRepository.save(createTestSubscriber("Bob", "Smith", "79217776655"));
        Subscriber subscriberTest = subscriberRepository.findByMsisdn(new BigInteger("79217776655")).orElseThrow(Exception::new);
        String url = "/api/v1/subscribers/" + subscriberTest.getId();

        mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.msisdn", is(79217776655L)));
    }

    @Test
    void givenSubscriberByIdWhenGetSubscriberThenStatus404() throws Exception {
        mvc.perform(get("/api/v1/subscribers/-102030")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void whenValidInputThenCreateSubscriberThenStatus201() throws Exception {
        Subscriber subscriberTest = createTestSubscriber("Bob", "Smith", "79217776655");

        mvc.perform(post("/api/v1/subscribers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(subscriberTest, SubscriberRequestDto.class))))
                .andDo(print())
                .andExpect(status().isCreated());

        List<Subscriber> subscribers = subscriberRepository.findAll();
        assertThat(subscribers).extracting(Subscriber::getMsisdn).contains(subscriberTest.getMsisdn());
    }

    @Test
    void whenInvalidInputThenCreateSubscriberThenStatus400() throws Exception {
        Subscriber subscriberTest = createTestSubscriber("Bob", "Smith", "79217776655");
        subscriberTest.setMsisdn(null);

        mvc.perform(post("/api/v1/subscribers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(subscriberTest, SubscriberRequestDto.class))))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidInputThenCreateSubscriberThenStatus409() throws Exception {
        Subscriber subscriberTest = createTestSubscriber("Bob", "Smith", "79217776655");
        subscriberRepository.save(subscriberTest);

        mvc.perform(post("/api/v1/subscribers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(subscriberTest, SubscriberRequestDto.class))))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    void whenValidInputThenUpdateSubscriberThenStatus200() throws Exception {
        subscriberRepository.save(createTestSubscriber("Bob", "Smith", "79217776655"));
        Subscriber subscriberTest = subscriberRepository.findByMsisdn(new BigInteger("79217776655")).orElseThrow(Exception::new);
        subscriberTest.setMsisdn(new BigInteger("79217776611"));
        subscriberMsisdns.add("79217776611");
        String url = "/api/v1/subscribers/" + subscriberTest.getId();

        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(subscriberTest, SubscriberRequestDto.class))))
                .andDo(print())
                .andExpect(status().isOk());

        List<Subscriber> subscribers = subscriberRepository.findAll();
        assertThat(subscribers).extracting(Subscriber::getMsisdn).contains(subscriberTest.getMsisdn());
    }

    @Test
    void whenInvalidInputThenUpdateSubscriberThenStatus404() throws Exception {
        Subscriber subscriberTest = createTestSubscriber("Bob", "Smith", "79217776655");

        mvc.perform(put("/api/v1/subscribers/-102030")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(subscriberTest, SubscriberRequestDto.class))))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void whenInvalidInputThenUpdateSubscriberThenStatus409() throws Exception {
        subscriberRepository.save(createTestSubscriber("Bob", "Smith", "79217776655"));
        subscriberRepository.save(createTestSubscriber("Jack", "Williams", "79217776644"));

        Subscriber subscriberTest = subscriberRepository.findByMsisdn(new BigInteger("79217776655")).orElseThrow(Exception::new);
        subscriberTest.setMsisdn(new BigInteger("79217776644"));
        subscriberMsisdns.add("79217776644");
        String url = "/api/v1/subscribers/" + subscriberTest.getId();

        mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modelMapper.map(subscriberTest, SubscriberRequestDto.class))))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    void whenValidInputThenCreateSubscriberActionThenStatus201() throws Exception {
        Subscriber subscriberTest = createTestSubscriber("Bob", "Smith", "79217776655");
        subscriberRepository.save(subscriberTest);
        subscriberTest = subscriberRepository.findByMsisdn(new BigInteger("79217776655")).orElseThrow(Exception::new);

        Action actionTest = createTestAction("test");
        actionRepository.save(actionTest);
        actionTest = actionRepository.findByName("test").orElseThrow(Exception::new);

        SubscriberActionRequestDto requestDto = SubscriberActionRequestDto.builder()
                .actionId(actionTest.getId())
                .cost(actionTest.getCost())
                .build();

        String url = "/api/v1/subscribers/" + subscriberTest.getId() + "/actions";

        mvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(requestDto)))
                .andDo(print())
                .andExpect(status().isCreated());

        List<SubscriberAction> subscriberActions = subscriberActionRepository.findAllBySubscriberId(subscriberTest.getId());
        assertThat(subscriberActions).extracting(SubscriberAction::getActionId).contains(actionTest.getId());
    }

    @Test
    void whenValidInputThenCreateSubscriberActionThenStatus200() throws Exception {
        Subscriber subscriberTest = createTestSubscriber("Bob", "Smith", "79217776655");
        subscriberRepository.save(subscriberTest);
        subscriberTest = subscriberRepository.findByMsisdn(new BigInteger("79217776655")).orElseThrow(Exception::new);

        Action actionTest = createTestAction("test");
        actionRepository.save(actionTest);
        actionTest = actionRepository.findByName("test").orElseThrow(Exception::new);

        SubscriberActionRequestDto requestDto = SubscriberActionRequestDto.builder()
                .actionId(actionTest.getId())
                .cost(actionTest.getCost())
                .build();

        String url = "/api/v1/subscribers/" + subscriberTest.getId() + "/actions";

        mvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(requestDto)))
                .andDo(print())
                .andExpect(status().isCreated());

        mvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(requestDto)))
                .andDo(print())
                .andExpect(status().isOk());

        List<SubscriberAction> subscriberActions = subscriberActionRepository.findAllBySubscriberId(subscriberTest.getId());
        assertThat(subscriberActions).extracting(SubscriberAction::getActionId).contains(actionTest.getId());
    }

    @Test
    void whenValidInputThenCreateSubscriberActionThenStatus404() throws Exception {
        SubscriberActionRequestDto requestDto = SubscriberActionRequestDto.builder()
                .actionId(-1)
                .build();

        String url = "/api/v1/subscribers/-1/actions";

        mvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(requestDto)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    private Subscriber createTestSubscriber(String firstName, String lastName, String msisdn) {
        subscriberMsisdns.add(msisdn);
        return Subscriber.builder()
                .firstName(firstName)
                .lastName(lastName)
                .balance(new BigDecimal("10"))
                .msisdn(new BigInteger(msisdn))
                .status(SubscriberStatus.ACTIVE)
                .build();
    }

    private Action createTestAction(String name) {
        actionNames.add(name);
        return Action.builder()
                .name(name)
                .cost(new BigDecimal("1.00"))
                .dayLimit(1)
                .type(ActionType.PAYMENT)
                .build();
    }
}
