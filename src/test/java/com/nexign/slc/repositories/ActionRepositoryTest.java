package com.nexign.slc.repositories;

import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.types.ActionType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
class ActionRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ActionRepository actionRepository;

    @Test
    void whenFindByNameThenReturnAction() {
        Action actionTest = Action.builder()
                .name("test")
                .cost(new BigDecimal("0"))
                .dayLimit(5)
                .type(ActionType.PAYMENT)
                .build();
        entityManager.persistAndFlush(actionTest);

        Action foundAction = actionRepository.findByName(actionTest.getName()).orElse(new Action());
        assertThat(foundAction.getName()).isEqualTo(actionTest.getName());
    }

    @Test
    void whenInvalidNameThenReturnNull() {
        Action actionFromDb = actionRepository.findByName("doesNotExist").orElse(null);
        assertThat(actionFromDb).isNull();
    }

    @Test
    void whenFindByIdThenReturnAction() {
        Action actionTest = Action.builder()
                .name("test")
                .cost(new BigDecimal("0"))
                .dayLimit(5)
                .type(ActionType.PAYMENT)
                .build();
        entityManager.persistAndFlush(actionTest);

        Action actionFromDb = actionRepository.findById(actionTest.getId()).orElse(new Action());
        assertThat(actionFromDb.getName()).isEqualTo(actionTest.getName());
    }

    @Test
    void whenInvalidIdThenReturnNull() {
        Action actionFromDb = actionRepository.findById(-1).orElse(null);
        assertThat(actionFromDb).isNull();
    }

    @Test
    void givenSetOfActionWhenFindAllThenReturnAllActions() {
        Action actionTest1 = Action.builder()
                .name("test_1")
                .cost(new BigDecimal("0.00"))
                .dayLimit(1)
                .type(ActionType.PAYMENT)
                .build();

        Action actionTest2 = Action.builder()
                .name("test_2")
                .cost(new BigDecimal("10000"))
                .dayLimit(2)
                .type(ActionType.PAYMENT)
                .build();

        Action actionTest3 = Action.builder()
                .name("test_3")
                .cost(new BigDecimal("222"))
                .dayLimit(3)
                .type(ActionType.PAYMENT)
                .build();

        entityManager.persist(actionTest1);
        entityManager.persist(actionTest2);
        entityManager.persist(actionTest3);
        entityManager.flush();

        List<Action> actions = actionRepository.findAll();

        assertThat(actions).extracting(Action::getName)
                .contains(actionTest1.getName(), actionTest2.getName(), actionTest3.getName());
    }
}
