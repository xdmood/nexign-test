package com.nexign.slc.repositories;

import com.nexign.slc.entities.Subscriber;
import com.nexign.slc.entities.types.SubscriberStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
class SubscriberRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SubscriberRepository subscriberRepository;

    @Test
    void whenFindByMsisdnThenReturnSubscriber() {
        Subscriber subscriberTest = Subscriber.builder()
                .firstName("Bob")
                .lastName("Smith")
                .balance(new BigDecimal("10"))
                .msisdn(new BigInteger("79217776655"))
                .status(SubscriberStatus.ACTIVE)
                .build();
        entityManager.persistAndFlush(subscriberTest);

        Subscriber foundSubscriber = subscriberRepository.findByMsisdn(subscriberTest.getMsisdn()).orElse(new Subscriber());
        assertThat(foundSubscriber.getMsisdn()).isEqualTo(subscriberTest.getMsisdn());
    }

    @Test
    void whenInvalidMsisdnThenReturnNull() {
        Subscriber subscriberFromDb = subscriberRepository.findByMsisdn(new BigInteger("79217776655")).orElse(null);
        assertThat(subscriberFromDb).isNull();
    }

    @Test
    void whenFindByIdThenReturnSubscriber() {
        Subscriber subscriberTest = Subscriber.builder()
                .firstName("Bob")
                .lastName("Smith")
                .balance(new BigDecimal("10"))
                .msisdn(new BigInteger("79217776655"))
                .status(SubscriberStatus.ACTIVE)
                .build();
        entityManager.persistAndFlush(subscriberTest);

        Subscriber subscriberFromDb = subscriberRepository.findById(subscriberTest.getId()).orElse(new Subscriber());
        assertThat(subscriberFromDb.getMsisdn()).isEqualTo(subscriberTest.getMsisdn());
    }

    @Test
    void whenInvalidIdThenReturnNull() {
        Subscriber subscriberFromDb = subscriberRepository.findById(-1).orElse(null);
        assertThat(subscriberFromDb).isNull();
    }

    @Test
    void givenSetOfSubscriberWhenFindAllThenReturnAllSubscribers() {
        Subscriber subscriberTest1 = Subscriber.builder()
                .firstName("Bob")
                .lastName("Smith")
                .balance(new BigDecimal("10"))
                .msisdn(new BigInteger("79217776655"))
                .status(SubscriberStatus.ACTIVE)
                .build();

        Subscriber subscriberTest2 = Subscriber.builder()
                .firstName("Jack")
                .lastName("Williams")
                .balance(new BigDecimal("100"))
                .msisdn(new BigInteger("79217776644"))
                .status(SubscriberStatus.ACTIVE)
                .build();

        Subscriber subscriberTest3 = Subscriber.builder()
                .firstName("Thomas")
                .lastName("Jones")
                .balance(new BigDecimal("1000"))
                .msisdn(new BigInteger("79217776633"))
                .status(SubscriberStatus.ACTIVE)
                .build();

        entityManager.persist(subscriberTest1);
        entityManager.persist(subscriberTest2);
        entityManager.persist(subscriberTest3);
        entityManager.flush();

        List<Subscriber> subscribers = subscriberRepository.findAll();

        assertThat(subscribers).extracting(Subscriber::getMsisdn)
                .contains(subscriberTest1.getMsisdn(), subscriberTest2.getMsisdn(), subscriberTest3.getMsisdn());
    }

}
