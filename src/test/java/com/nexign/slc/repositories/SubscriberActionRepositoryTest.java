package com.nexign.slc.repositories;

import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.Subscriber;
import com.nexign.slc.entities.SubscriberAction;
import com.nexign.slc.entities.types.ActionType;
import com.nexign.slc.entities.types.SubscriberStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class SubscriberActionRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private SubscriberRepository subscriberRepository;

    @Autowired
    private SubscriberActionRepository subscriberActionRepository;

    @Test
    void whenFindBySubscriberIdThenReturnSubscriberActions() {
        Subscriber subscriberTest = Subscriber.builder()
                .firstName("Bob")
                .lastName("Smith")
                .balance(new BigDecimal("10"))
                .msisdn(new BigInteger("79217776655"))
                .status(SubscriberStatus.ACTIVE)
                .build();
        entityManager.persistAndFlush(subscriberTest);
        Subscriber foundSubscriber = subscriberRepository.findByMsisdn(subscriberTest.getMsisdn()).orElse(new Subscriber());


        Action actionTest = Action.builder()
                .name("test")
                .cost(new BigDecimal("0"))
                .dayLimit(5)
                .type(ActionType.PAYMENT)
                .build();
        entityManager.persistAndFlush(actionTest);
        Action foundAction = actionRepository.findByName(actionTest.getName()).orElse(new Action());

        SubscriberAction subscriberAction = SubscriberAction.builder()
                .subscriberId(foundSubscriber.getId())
                .actionId(foundAction.getId())
                .actionDate(LocalDate.now())
                .actionTime(LocalTime.now())
                .cost(foundAction.getCost())
                .build();
        entityManager.persistAndFlush(subscriberAction);

        List<SubscriberAction> subscriberActionsBySubscriberId = subscriberActionRepository
                .findAllBySubscriberId(foundSubscriber.getId());
        assertThat(subscriberActionsBySubscriberId).extracting(SubscriberAction::getActionId)
                .contains(foundAction.getId());

        List<SubscriberAction> subscriberActionsByActionIdAndSubscriberId = subscriberActionRepository
                .findAllByActionIdAndSubscriberId(foundAction.getId(), foundSubscriber.getId());
        assertThat(subscriberActionsByActionIdAndSubscriberId).extracting(SubscriberAction::getActionTime)
                .contains(subscriberAction.getActionTime());

        List<SubscriberAction> subscriberActionsByActionIdAndSubscriberIdAndActionDate = subscriberActionRepository
                .findAllByActionIdAndSubscriberIdAndActionDate(foundAction.getId(), foundSubscriber.getId(), subscriberAction.getActionDate());
        assertThat(subscriberActionsByActionIdAndSubscriberIdAndActionDate).extracting(SubscriberAction::getActionTime)
                .contains(subscriberAction.getActionTime());

    }

    @Test
    void whenInvalidBySubscriberIdThenReturnEmptyList() {
        List<SubscriberAction> subscriberActions = subscriberActionRepository.findAllBySubscriberId(-1);
        assertThat(subscriberActions).isEmpty();

        List<SubscriberAction> subscriberActions1 = subscriberActionRepository
                .findAllByActionIdAndSubscriberId(-1, -1);
        assertThat(subscriberActions1).isEmpty();

        List<SubscriberAction> subscriberActions2 = subscriberActionRepository
                .findAllByActionIdAndSubscriberIdAndActionDate(-1, -1, LocalDate.now());
        assertThat(subscriberActions2).isEmpty();
    }
}
