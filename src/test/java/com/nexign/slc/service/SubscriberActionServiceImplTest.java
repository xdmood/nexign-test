package com.nexign.slc.service;

import com.nexign.slc.entities.SubscriberAction;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.repositories.ActionRepository;
import com.nexign.slc.repositories.SubscriberActionRepository;
import com.nexign.slc.repositories.SubscriberRepository;
import com.nexign.slc.services.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class SubscriberActionServiceImplTest {

    @TestConfiguration
    static class SubscriberActionServiceImplTestContextConfiguration {
        @Bean
        public SubscriberService subscriberService() {
            return new SubscriberServiceImpl();
        }

        @Bean
        public ActionService actionService() {
            return new ActionServiceImpl();
        }


        @Bean
        public SubscriberActionService subscriberActionService() {
            return new SubscriberActionServiceImpl();
        }
    }

    @Autowired
    private SubscriberActionService subscriberActionService;

    @MockBean
    private SubscriberActionRepository subscriberActionRepository;

    @MockBean
    private SubscriberRepository subscriberRepository;

    @MockBean
    private ActionRepository actionRepository;

    @BeforeEach
    void setUp() {

        SubscriberAction subscriberActionTest1 = SubscriberAction.builder()
                .subscriberId(1)
                .actionId(1)
                .actionDate(LocalDate.now())
                .actionTime(LocalTime.now())
                .cost(new BigDecimal(10))
                .build();

        SubscriberAction subscriberActionTest2 = SubscriberAction.builder()
                .subscriberId(1)
                .actionId(1)
                .actionDate(LocalDate.now())
                .actionTime(LocalTime.now())
                .cost(new BigDecimal(10))
                .build();

        SubscriberAction subscriberActionTest3 = SubscriberAction.builder()
                .subscriberId(1)
                .actionId(1)
                .actionDate(LocalDate.now())
                .actionTime(LocalTime.now())
                .cost(new BigDecimal(10))
                .build();

        SubscriberAction subscriberActionTest4 = SubscriberAction.builder()
                .subscriberId(1)
                .actionId(2)
                .actionDate(LocalDate.now())
                .actionTime(LocalTime.now())
                .cost(new BigDecimal(10))
                .build();

        SubscriberAction subscriberActionTest5 = SubscriberAction.builder()
                .subscriberId(1)
                .actionId(2)
                .actionDate(LocalDate.now())
                .actionTime(LocalTime.now())
                .cost(new BigDecimal(10))
                .build();


        List<SubscriberAction> subscriberActions1 = Arrays.asList(
                subscriberActionTest1,
                subscriberActionTest2,
                subscriberActionTest3,
                subscriberActionTest4,
                subscriberActionTest5);

        List<SubscriberAction> subscriberActions2 = Arrays.asList(
                subscriberActionTest1,
                subscriberActionTest2,
                subscriberActionTest3);

        Mockito.when(subscriberActionRepository.findAllBySubscriberId(subscriberActionTest1.getSubscriberId())).thenReturn(subscriberActions1);
        Mockito.when(subscriberActionRepository.findAllByActionIdAndSubscriberId(subscriberActionTest1.getActionId(), subscriberActionTest1.getSubscriberId())).thenReturn(subscriberActions2);
        Mockito.when(subscriberActionRepository.findAllByActionIdAndSubscriberIdAndActionDate(subscriberActionTest1.getActionId(), subscriberActionTest1.getSubscriberId(), subscriberActionTest1.getActionDate())).thenReturn(subscriberActions2);
    }

    @Test
    void whenValidSubscriberActionThenSubscriberActionShouldBeFound() {
        List<SubscriberAction> subscriberActions1 = subscriberActionService.getSubscriberActions(1);
        assertThat(subscriberActions1).extracting(SubscriberAction::getActionId).contains(1, 2);

        List<SubscriberAction> subscriberActions2 = subscriberActionService.getSubscriberActions(1, 1);
        assertThat(subscriberActions2).extracting(SubscriberAction::getActionId).containsOnly(1);

        List<SubscriberAction> subscriberActions3 = subscriberActionService.getSubscriberActions(1, 1, LocalDate.now());
        assertThat(subscriberActions3).extracting(SubscriberAction::getActionId).containsOnly(1);
    }

    @Test
    void whenInvalidSubscriberActionThenSubscriberActionShouldNotBeFound() {
        List<SubscriberAction> subscriberActions1 = subscriberActionService.getSubscriberActions(-1);
        assertThat(subscriberActions1).isEmpty();

        List<SubscriberAction> subscriberActions2 = subscriberActionService.getSubscriberActions(-1, -1);
        assertThat(subscriberActions2).isEmpty();

        List<SubscriberAction> subscriberActions3 = subscriberActionService.getSubscriberActions(-1, -1, LocalDate.now());
        assertThat(subscriberActions3).isEmpty();
    }
}
