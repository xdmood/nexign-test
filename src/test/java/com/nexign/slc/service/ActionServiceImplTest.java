package com.nexign.slc.service;

import com.nexign.slc.entities.Action;
import com.nexign.slc.entities.types.ActionType;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.repositories.ActionRepository;
import com.nexign.slc.services.ActionService;
import com.nexign.slc.services.ActionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class ActionServiceImplTest {

    @TestConfiguration
    static class ActionServiceImplTestContextConfiguration {
        @Bean
        public ActionService actionService() {
            return new ActionServiceImpl();
        }
    }

    @Autowired
    private ActionService actionService;

    @MockBean
    private ActionRepository actionRepository;

    @BeforeEach
    void setUp() {

        Action actionTest1 = Action.builder()
                .id(100)
                .name("test_1")
                .cost(new BigDecimal("0.00"))
                .dayLimit(1)
                .type(ActionType.PAYMENT)
                .build();

        Action actionTest2 = Action.builder()
                .id(101)
                .name("test_2")
                .cost(new BigDecimal("10000"))
                .dayLimit(2)
                .type(ActionType.PAYMENT)
                .build();

        Action actionTest3 = Action.builder()
                .id(102)
                .name("test_3")
                .cost(new BigDecimal("222"))
                .dayLimit(3)
                .type(ActionType.PAYMENT)
                .build();

        List<Action> actions = Arrays.asList(actionTest1, actionTest2, actionTest3);

        Mockito.when(actionRepository.findByName(actionTest1.getName())).thenReturn(Optional.of(actionTest1));
        Mockito.when(actionRepository.findByName(actionTest3.getName())).thenReturn(Optional.of(actionTest3));
        Mockito.when(actionRepository.findByName("wrong_name")).thenReturn(Optional.empty());
        Mockito.when(actionRepository.findById(actionTest1.getId())).thenReturn(Optional.of(actionTest1));
        Mockito.when(actionRepository.findById(-1)).thenReturn(Optional.empty());
        Mockito.when(actionRepository.findAll()).thenReturn(actions);

    }

    @Test
    void whenValidNameThenActionShouldBeFound() throws SlcException {
        Action actionFromDb = actionService.getAction("test_3");
        assertThat(actionFromDb.getName()).isEqualTo("test_3");
    }

    @Test
    void whenInvalidNameThenActionShouldNotBeFound() {
        assertThrows(SlcException.class, () -> actionService.getAction("wrong_name"));
        verifyFindByNameIsCalledOnce("wrong_name");
    }

    @Test
    void whenValidIdThenActionShouldBeFound() throws SlcException {
        Action actionFromDb = actionService.getAction(100);
        assertThat(actionFromDb.getName()).isEqualTo("test_1");
        verifyFindByIdIsCalledOnce();
    }

    @Test
    void whenInvalidIdThenActionShouldNotBeFound() {
        assertThrows(SlcException.class, () -> actionService.getAction(-1));
        verifyFindByIdIsCalledOnce();
    }

    @Test
    void givenActionsWhenGetAllThenReturnRecords() {
        List<Action> actions = actionService.getActions();
        verifyFindAllActionsIsCalledOnce();
        assertThat(actions).extracting(Action::getName).contains("test_1", "test_2", "test_3");
    }

    private void verifyFindByNameIsCalledOnce(String name) {
        Mockito.verify(actionRepository, VerificationModeFactory.times(1)).findByName(name);
        Mockito.reset(actionRepository);
    }

    private void verifyFindByIdIsCalledOnce() {
        Mockito.verify(actionRepository, VerificationModeFactory.times(1)).findById(Mockito.anyInt());
        Mockito.reset(actionRepository);
    }

    private void verifyFindAllActionsIsCalledOnce() {
        Mockito.verify(actionRepository, VerificationModeFactory.times(1)).findAll();
        Mockito.reset(actionRepository);
    }
}
