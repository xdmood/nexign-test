package com.nexign.slc.service;

import com.nexign.slc.entities.Subscriber;
import com.nexign.slc.entities.types.SubscriberStatus;
import com.nexign.slc.exception.SlcException;
import com.nexign.slc.repositories.SubscriberRepository;
import com.nexign.slc.services.SubscriberService;
import com.nexign.slc.services.SubscriberServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
class SubscriberServiceImplTest {

    @TestConfiguration
    static class SubscriberServiceImplTestContextConfiguration {
        @Bean
        public SubscriberService subscriberService() {
            return new SubscriberServiceImpl();
        }
    }

    @Autowired
    private SubscriberService subscriberService;

    @MockBean
    private SubscriberRepository subscriberRepository;

    @BeforeEach
    void setUp() {

        Subscriber subscriberTest1 = Subscriber.builder()
                .id(100)
                .firstName("Bob")
                .lastName("Smith")
                .balance(new BigDecimal("10"))
                .msisdn(new BigInteger("79217776655"))
                .status(SubscriberStatus.ACTIVE)
                .build();

        Subscriber subscriberTest2 = Subscriber.builder()
                .id(101)
                .firstName("Jack")
                .lastName("Williams")
                .balance(new BigDecimal("100"))
                .msisdn(new BigInteger("79217776644"))
                .status(SubscriberStatus.ACTIVE)
                .build();

        Subscriber subscriberTest3 = Subscriber.builder()
                .id(102)
                .firstName("Thomas")
                .lastName("Jones")
                .balance(new BigDecimal("1000"))
                .msisdn(new BigInteger("79217776633"))
                .status(SubscriberStatus.ACTIVE)
                .build();

        List<Subscriber> subscribers = Arrays.asList(subscriberTest1, subscriberTest2, subscriberTest3);

        Mockito.when(subscriberRepository.findByMsisdn(subscriberTest1.getMsisdn())).thenReturn(Optional.of(subscriberTest1));
        Mockito.when(subscriberRepository.findByMsisdn(subscriberTest3.getMsisdn())).thenReturn(Optional.of(subscriberTest3));
        Mockito.when(subscriberRepository.findByMsisdn(new BigInteger("79217776600"))).thenReturn(Optional.empty());
        Mockito.when(subscriberRepository.findById(subscriberTest1.getId())).thenReturn(Optional.of(subscriberTest1));
        Mockito.when(subscriberRepository.findById(-1)).thenReturn(Optional.empty());
        Mockito.when(subscriberRepository.findAll()).thenReturn(subscribers);

    }

    @Test
    void whenValidMsisdnThenSubscriberShouldBeFound() throws SlcException {
        Subscriber subscriberFromDb = subscriberService.getSubscriberByMsisdn(new BigInteger("79217776633"));
        assertThat(subscriberFromDb.getMsisdn()).isEqualTo(new BigInteger("79217776633"));
    }

    @Test
    void whenInvalidMsisdnThenSubscriberShouldNotBeFound() {
        assertThrows(SlcException.class, () -> subscriberService.getSubscriberByMsisdn(new BigInteger("79217776600")));
        verifyFindByMsisdnIsCalledOnce(new BigInteger("79217776600"));
    }

    @Test
    void whenValidIdThenSubscriberShouldBeFound() throws SlcException {
        Subscriber subscriberFromDb = subscriberService.getSubscriber(100);
        assertThat(subscriberFromDb.getMsisdn()).isEqualTo(new BigInteger("79217776655"));
        verifyFindByIdIsCalledOnce();
    }

    @Test
    void whenInvalidIdThenSubscriberShouldNotBeFound() {
        assertThrows(SlcException.class, () -> subscriberService.getSubscriber(-1));
        verifyFindByIdIsCalledOnce();
    }

    @Test
    void givenSubscribersWhenGetAllThenReturnRecords() {
        List<Subscriber> subscribers = subscriberService.getSubscribers();
        verifyFindAllSubscribersIsCalledOnce();
        assertThat(subscribers).extracting(Subscriber::getMsisdn)
                .contains(new BigInteger("79217776655"), new BigInteger("79217776644"), new BigInteger("79217776633"));
    }

    private void verifyFindByMsisdnIsCalledOnce(BigInteger msisdn) {
        Mockito.verify(subscriberRepository, VerificationModeFactory.times(1)).findByMsisdn(msisdn);
        Mockito.reset(subscriberRepository);
    }

    private void verifyFindByIdIsCalledOnce() {
        Mockito.verify(subscriberRepository, VerificationModeFactory.times(1)).findById(Mockito.anyInt());
        Mockito.reset(subscriberRepository);
    }

    private void verifyFindAllSubscribersIsCalledOnce() {
        Mockito.verify(subscriberRepository, VerificationModeFactory.times(1)).findAll();
        Mockito.reset(subscriberRepository);
    }
}
