# Техническое задание «Жизненный цикл абонента»
-------------
## DB
-------------
В приложении в качетве БД использован PostrgeSQL (проверно на 9.6 и 10.7).
Параметры подключения в конфиге application.yml:

* url: jdbc:postgresql://127.0.0.1:5432/nx
* username: postgres
* password: 12345678


В каталоге /sql находятся скрипты для создания схемы БД и заполнения тестовыми данными:

* slc_schema.sql - для создания схемы
* slc_data.sql - для заполнения данными
  
В БД все сущности находся в схеме slc:

* slc.subscriber - Абонент
* slc.action - Действия доступные абоненту
* slc.subscriber_action - События, совершённые абонентом


-------------
## REST API
-------------
Приложение проверено на Tomcat 9.0.22

#### Swagger доступен по ссылке: `{{URL}}/slc/swagger-ui.html#/`

### Действия доступные абоненту (Action)

**Параметры:**

 * name - уникальное наименование
 * cost - стоимость действия
 * dayLimit - лимит на действие в сутки (если отрицательное число, то лимита нет)
 * type - тип действия  ( PAYMENT- оплата (звонок, смс), REPLENISHMENT - поплнение (пополнение баланса))

**Список типов действий доступных абоненту:** GET `{{URL}}/api/v1/actions/types`

Response code:

 * 200 - OK
 * 400 - Something went wrong
 * 404 - Resource not found
 * 500 - Something went wrong in server layer

Пример ответа:
```json
[
  "PAYMENT",
  "REPLENISHMENT"
]
```

**Список действий доступных абоненту:** GET `{{URL}}/api/v1/actions`

Response code:

 * 200 - OK
 * 400 - Something went wrong
 * 404 - Resource not found
 * 500 - Something went wrong in server layer

Пример ответа:
```json
[
  {
    "id": 1,
    "name": "call",
    "cost": 2,
    "dayLimit": 5,
    "type": "PAYMENT"
  },
  {
    "id": 2,
    "name": "sms",
    "cost": 1,
    "dayLimit": -1,
    "type": "PAYMENT"
  },
  {
    "id": 3,
    "name": "replenishment",
    "cost": 0,
    "dayLimit": -1,
    "type": "REPLENISHMENT"
  }
]
```



**Получить действие по "id":** GET `{{URL}}/api/v1/actions{id}`

Response code:

 * 200 - OK
 * 400 - Something went wrong
 * 404 - Resource not found
 * 500 - Something went wrong in server layer

Пример ответа:
```json
{
  "id": 1,
  "name": "call",
  "cost": 2,
  "dayLimit": 5,
  "type": "PAYMENT"
}
```

**Создать действие:** POST `{{URL}}/api/v1/actions`

Response code:

 * 201 - Created
 * 400 - Something went wrong
 * 404 - Resource not found
 * 409 - Conflict - name must be unique
 * 500 - Something went wrong in server layer
 
 Пример запроса:
```json
{
  "name": "test",
  "cost": 3,
  "dayLimit": 5,
  "type": "PAYMENT"
}
```

Пример ответа:
```json
{
  "id": 37,
  "name": "test",
  "cost": 3,
  "dayLimit": 5,
  "type": "PAYMENT"
}
```

Пример ответа при ошибке 409 (название заянто):
```json
{
  "timestamp": "2019-08-11T19:24:05.319+0000",
  "status": 409,
  "error": "Conflict",
  "message": "An entity with the same \"name\" already exists!",
  "path": "/slc/api/v1/actions"
}
```

**Обновить атрибуты действия:** PUT `{{URL}}/api/v1/actions/{id}`

Response code:

 * 200 - OK
 * 400 - Something went wrong
 * 404 - Resource not found
 * 409 - Conflict - name must be unique
 * 500 - Something went wrong in server layer
 
 Пример запроса:
```json
{
  "name": "test_test",
  "cost": 5,
  "dayLimit": -1,
  "type": "PAYMENT"
}
```

Пример ответа:
```json
{
  "id": 37,
  "name": "test_test",
  "cost": 5,
  "dayLimit": -1,
  "type": "PAYMENT"
}
```

### Абонент (Subscriber)

**Параметры:**

 * firstName - Имя
 * lastName - Фамилия
 * msisdn - номер телефона (ограничения по количеству символов в конфиге: slc:subscriber:msisdn:length:min: 7 ...max:15)
 * balance - баланс
 * status - статус  (ACTIVE - при положительном балансе, BLOCKED - при отрицательном, разрешены только действия поплнения)

**Список абонентов:** GET `{{URL}}/api/v1/subscribers`

Response code:

 * 200 - OK
 * 400 - Something went wrong
 * 404 - Resource not found
 * 500 - Something went wrong in server layer

Пример ответа:
```json
[
  {
    "id": 1,
    "firstName": "Иван",
    "lastName": "Иванов",
    "msisdn": 79217822001,
    "balance": 10,
    "status": "ACTIVE"
  },
  {
    "id": 2,
    "firstName": "Сергей",
    "lastName": "Сергеев",
    "msisdn": 79217822002,
    "balance": 12,
    "status": "ACTIVE"
  }
]
```

**Получить абонента по "id":** GET `{{URL}}/api/v1/subscribers{id}`

Response code:

 * 200 - OK
 * 400 - Something went wrong
 * 404 - Resource not found
 * 500 - Something went wrong in server layer

Пример ответа:
```json
{
  "id": 1,
  "firstName": "Иван",
  "lastName": "Иванов",
  "msisdn": 79217822001,
  "balance": 10,
  "status": "ACTIVE"
}
```

**Создать абонента:** POST `{{URL}}/api/v1/subscribers`

Response code:

 * 201 - Created
 * 400 - Something went wrong
 * 404 - Resource not found
 * 409 - Conflict - name must be unique
 * 500 - Something went wrong in server layer
 
 Пример запроса:
```json
{
  "firstName": "test",
  "lastName": "test",
  "msisdn": 7896655321
}
```

Пример ответа:
```json
{
  "id": 44,
  "firstName": "test",
  "lastName": "test",
  "msisdn": 7896655321,
  "balance": 0,
  "status": "ACTIVE"
}
```

Пример ответа при ошибке 409 (номер занят):
```json
{
  "timestamp": "2019-08-11T19:40:55.492+0000",
  "status": 409,
  "error": "Conflict",
  "message": "An entity with the same \"msisdn\" already exists!",
  "path": "/slc/api/v1/subscribers"
}
```

Пример ответа при ошибке 400 (номер слишком короткий):
```json
{
  "timestamp": "2019-08-11T19:42:34.843+0000",
  "status": 400,
  "error": "Bad Request",
  "message": "Values cannot be null, empty or negative value: msisdn have wrong length!",
  "path": "/slc/api/v1/subscribers"
}
```


**Обновить атрибуты абонента:** PUT `{{URL}}/api/v1/subscribers{id}`

Response code:

 * 200 - OK
 * 400 - Something went wrong
 * 404 - Resource not found
 * 409 - Conflict - name must be unique
 * 500 - Something went wrong in server layer
 
 Пример запроса:
```json
{
  "firstName": "test_test",
  "lastName": "test_test",
  "msisdn": 7891112233
}
```

Пример ответа:
```json
{
  "id": 44,
  "firstName": "test_test",
  "lastName": "test_test",
  "msisdn": 7891112233,
  "balance": 0,
  "status": "ACTIVE"
}
```

### Выполнения действий абонентом (SubscriberAction)

**Параметры:**

 * subscriberId - id абонента
 * actionId - id действия
 * cost - стоимость действия
 * actionDate - дата совершения действия
 * actionTime - время совершения действия
 
 **Получить список действий абонента по "id" абонента:** GET `{{URL}}/api/v1/subscribers/{id}/actions`

Response code:

 * 200 - OK
 * 400 - Something went wrong
 * 404 - Resource not found
 * 500 - Something went wrong in server layer

Пример ответа:
```json
[
  {
    "id": 2,
    "subscriberId": 1,
    "actionId": 1,
    "cost": 2,
    "actionDate": "2019-08-07",
    "actionTime": "12:00:00"
  },
  {
    "id": 3,
    "subscriberId": 1,
    "actionId": 1,
    "cost": 2,
    "actionDate": "2019-08-07",
    "actionTime": "12:00:00"
  }
]
```

**Создать действие абонента (звонок, смс, поплнение баланса):** POST `{{URL}}/api/v1/subscribers/{id}/actions`

Параметр 'cost' необходимо указать, только для действия поплнения баланса (REPLENISHMENT). 

Response code:

 * 200 - Subscriber action not created, subscriber have status: BLOCKED
 * 201 - Subscriber action created
 * 400 - Something went wrong
 * 404 - Resource not found
 * 500 - Something went wrong in server layer
 
 Пример запроса списания (звонок):
```json
{
  "actionId": 1
}
```

 Пример запроса поплнения баланса:
```json
{
  "actionId": 3,
  "cost": 100
}
```

Пример ответа 201:
```json
{
  "result": true,
  "message": "ok"
}
```

Пример ответа 200:
```json
{
  "result": false,
  "message": "Subscriber status: BLOCKED"
}
```