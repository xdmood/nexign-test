--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7
-- Dumped by pg_dump version 10.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: slc; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA slc;


ALTER SCHEMA slc OWNER TO postgres;

--
-- Name: check_balance_and_change_status(); Type: FUNCTION; Schema: slc; Owner: postgres
--

CREATE FUNCTION slc.check_balance_and_change_status() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	BEGIN	
		IF (NEW.balance < 0) THEN
			NEW.status = 'BLOCKED'::character varying;
		ELSE
			NEW.status = 'ACTIVE'::character varying;
		end IF;
		return NEW;
	END;
$$;


ALTER FUNCTION slc.check_balance_and_change_status() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: action; Type: TABLE; Schema: slc; Owner: postgres
--

CREATE TABLE slc.action (
    id integer NOT NULL,
    name text NOT NULL,
    cost numeric DEFAULT 0,
    day_limit integer NOT NULL,
    type character varying(16) NOT NULL
);


ALTER TABLE slc.action OWNER TO postgres;

--
-- Name: action_id_seq; Type: SEQUENCE; Schema: slc; Owner: postgres
--

CREATE SEQUENCE slc.action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE slc.action_id_seq OWNER TO postgres;

--
-- Name: action_id_seq; Type: SEQUENCE OWNED BY; Schema: slc; Owner: postgres
--

ALTER SEQUENCE slc.action_id_seq OWNED BY slc.action.id;


--
-- Name: subscriber; Type: TABLE; Schema: slc; Owner: postgres
--

CREATE TABLE slc.subscriber (
    id integer NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    msisdn bigint NOT NULL,
    balance numeric DEFAULT 0,
    status character varying(16) DEFAULT 'ACTIVE'::character varying
);


ALTER TABLE slc.subscriber OWNER TO postgres;

--
-- Name: subscriber_action; Type: TABLE; Schema: slc; Owner: postgres
--

CREATE TABLE slc.subscriber_action (
    id bigint NOT NULL,
    subscriber_id integer,
    action_id integer,
    cost numeric NOT NULL,
    action_time time without time zone DEFAULT now() NOT NULL,
    action_date date DEFAULT now() NOT NULL
);


ALTER TABLE slc.subscriber_action OWNER TO postgres;

--
-- Name: subscriber_action_id_seq; Type: SEQUENCE; Schema: slc; Owner: postgres
--

CREATE SEQUENCE slc.subscriber_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE slc.subscriber_action_id_seq OWNER TO postgres;

--
-- Name: subscriber_action_id_seq; Type: SEQUENCE OWNED BY; Schema: slc; Owner: postgres
--

ALTER SEQUENCE slc.subscriber_action_id_seq OWNED BY slc.subscriber_action.id;


--
-- Name: subscriber_id_seq; Type: SEQUENCE; Schema: slc; Owner: postgres
--

CREATE SEQUENCE slc.subscriber_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE slc.subscriber_id_seq OWNER TO postgres;

--
-- Name: subscriber_id_seq; Type: SEQUENCE OWNED BY; Schema: slc; Owner: postgres
--

ALTER SEQUENCE slc.subscriber_id_seq OWNED BY slc.subscriber.id;


--
-- Name: action id; Type: DEFAULT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.action ALTER COLUMN id SET DEFAULT nextval('slc.action_id_seq'::regclass);


--
-- Name: subscriber id; Type: DEFAULT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.subscriber ALTER COLUMN id SET DEFAULT nextval('slc.subscriber_id_seq'::regclass);


--
-- Name: subscriber_action id; Type: DEFAULT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.subscriber_action ALTER COLUMN id SET DEFAULT nextval('slc.subscriber_action_id_seq'::regclass);


--
-- Name: action action_name_key; Type: CONSTRAINT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.action
    ADD CONSTRAINT action_name_key UNIQUE (name);


--
-- Name: action action_pkey; Type: CONSTRAINT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.action
    ADD CONSTRAINT action_pkey PRIMARY KEY (id);


--
-- Name: subscriber_action subscriber_action_pkey; Type: CONSTRAINT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.subscriber_action
    ADD CONSTRAINT subscriber_action_pkey PRIMARY KEY (id);


--
-- Name: subscriber subscriber_msisdn_key; Type: CONSTRAINT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.subscriber
    ADD CONSTRAINT subscriber_msisdn_key UNIQUE (msisdn);


--
-- Name: subscriber subscriber_pkey; Type: CONSTRAINT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.subscriber
    ADD CONSTRAINT subscriber_pkey PRIMARY KEY (id);


--
-- Name: subscriber tr_check_balance_and_change_status; Type: TRIGGER; Schema: slc; Owner: postgres
--

CREATE TRIGGER tr_check_balance_and_change_status BEFORE UPDATE ON slc.subscriber FOR EACH ROW WHEN ((old.balance IS DISTINCT FROM new.balance)) EXECUTE PROCEDURE slc.check_balance_and_change_status();


--
-- Name: subscriber_action subscriber_action_action_id_fkey; Type: FK CONSTRAINT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.subscriber_action
    ADD CONSTRAINT subscriber_action_action_id_fkey FOREIGN KEY (action_id) REFERENCES slc.action(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: subscriber_action subscriber_action_subscriber_id_fkey; Type: FK CONSTRAINT; Schema: slc; Owner: postgres
--

ALTER TABLE ONLY slc.subscriber_action
    ADD CONSTRAINT subscriber_action_subscriber_id_fkey FOREIGN KEY (subscriber_id) REFERENCES slc.subscriber(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

