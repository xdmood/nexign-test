--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7
-- Dumped by pg_dump version 10.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: action; Type: TABLE DATA; Schema: slc; Owner: postgres
--

INSERT INTO slc.action (id, name, cost, day_limit, type) VALUES (1, 'call', 2.00, 5, 'PAYMENT');
INSERT INTO slc.action (id, name, cost, day_limit, type) VALUES (2, 'sms', 1.00, -1, 'PAYMENT');
INSERT INTO slc.action (id, name, cost, day_limit, type) VALUES (3, 'replenishment', 0.00, -1, 'REPLENISHMENT');


--
-- Data for Name: subscriber; Type: TABLE DATA; Schema: slc; Owner: postgres
--

INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (1, 'Иван', 'Иванов', 79217822001, 10, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (2, 'Сергей', 'Сергеев', 79217822002, 12, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (3, 'Алексей', 'Алексеев', 79217822003, 15, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (4, 'Пётр', 'Петров', 79217822004, 0, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (5, 'Александр', 'Александров', 79217822005, 0, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (6, 'Василий', 'Васильев', 79217822006, 5, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (7, 'Елисей', 'Елисеев', 79217822007, 2, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (8, 'Захар', 'Захаров', 79217822008, 1, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (9, 'Андрей', 'Андреев', 79217822009, 20, 'ACTIVE');
INSERT INTO slc.subscriber (id, first_name, last_name, msisdn, balance, status) VALUES (10, 'Дмитрий', 'Дмитриев', 79217822010, 25, 'ACTIVE');


--
-- Data for Name: subscriber_action; Type: TABLE DATA; Schema: slc; Owner: postgres
--

INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (2, 1, 1, 2.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (3, 1, 1, 2.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (1, 1, 2, 1.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (4, 1, 2, 1.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (5, 1, 1, 2.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (6, 1, 1, 2.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (7, 1, 1, 2.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (8, 2, 1, 2.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (9, 2, 2, 1.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (10, 2, 1, 2.00, '12:00:00', '2019-08-07');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (11, 1, 2, 1.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (12, 1, 1, 2.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (13, 1, 1, 2.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (14, 1, 2, 1.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (15, 1, 1, 2.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (16, 1, 1, 2.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (17, 1, 1, 2.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (18, 2, 1, 2.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (19, 2, 2, 1.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (20, 2, 1, 2.00, '12:00:00', '2019-08-08');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (21, 1, 2, 1.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (22, 1, 1, 2.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (23, 1, 1, 2.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (24, 1, 2, 1.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (25, 1, 1, 2.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (26, 1, 1, 2.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (27, 1, 1, 2.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (28, 2, 1, 2.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (29, 2, 2, 1.00, '12:00:00', '2019-08-09');
INSERT INTO slc.subscriber_action (id, subscriber_id, action_id, cost, action_time, action_date) VALUES (30, 2, 1, 2.00, '12:00:00', '2019-08-09');


--
-- Name: action_id_seq; Type: SEQUENCE SET; Schema: slc; Owner: postgres
--

SELECT pg_catalog.setval('slc.action_id_seq', 4, true);


--
-- Name: subscriber_action_id_seq; Type: SEQUENCE SET; Schema: slc; Owner: postgres
--

SELECT pg_catalog.setval('slc.subscriber_action_id_seq', 31, true);


--
-- Name: subscriber_id_seq; Type: SEQUENCE SET; Schema: slc; Owner: postgres
--

SELECT pg_catalog.setval('slc.subscriber_id_seq', 11, true);


--
-- PostgreSQL database dump complete
--

